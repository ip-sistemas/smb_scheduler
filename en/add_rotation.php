<?php
require_once './debug.php';
require_once '../inc/config.inc.php';
require_once '../inc/conex_db_mb.php';

/**
 * Devuelve $datos[$key] si está en el array $datos
 * @param  mixed $id    $id a buscar. Casi siempre un número
 * @param  array $datos Array donde se busca la información
 * @param  string $key   Clave del resultado
 * @return mixed        El valor encontrado. Cadena vacía si no encuentra nada
 */
function dato_from_id($id, $datos, $key)
{
	$salida = '';
	if ($id != '') {
		foreach ($datos as $linea) {
			if ($linea['id'] == $id) {
				$salida = $linea[$key];
				break;
			}
		}
	}
	return $salida;
}

// pkg_trunk (id, providerip) tienes todos los trunks
$trunks = sql2array_mb('SELECT id, providerip FROM pkg_trunk ORDER BY providerip ASC');
$trunks_opt = '';
foreach ($trunks as $row) {
	$trunks_opt .= "<option value='{$row['id']}'>{$row['providerip']}</option>";
}
// pkg_trunk_group (id, name) Grupos de trunks
$trunk_groups = sql2array_mb('SELECT id, name FROM pkg_trunk_group ORDER BY name ASC');
$trunk_groups_opt = '';
foreach ($trunk_groups as $row) {
	$trunk_groups_opt .= "<option value='{$row['id']}'>{$row['name']}</option>";
}

// pkg_trunk_group_trunk (id, id_trunk_group, id_trunk) Relacion
//$trunk_trunkgroup = sql2array_mb('SELECT id, id_trunk_group, id_trunk FROM pkg_trunk_group_trunk ORDER BY id_trunk_group ASC');

$tipo = [
	'TIME' => 'By Time',
	'PER' => 'By Period',
	'LIM' => 'By Limit',
];

$campos['TIME'] = <<<LRDTAB
<div class="input-group">
<label for="time[]">Program:</label>
<input type="text" name="time[]" id="time[]" class="time-pickable" autocomplete="false" required>
</div>
LRDTAB;

$campos['PER'] = <<<LRDTAB
<div class="input-group">
<label for="per[]">Program (min):</label>
<input type="number" name="per[]" id="per[]" min="1" step="1" required>
</div>
LRDTAB;

$campos['LIM'] = <<<LRDTAB
<div class="input-group inline-block">
<label for="trunk[]">Trunk:</label>
<select name="trunk[]" id="trunk[]">
{$trunks_opt}
</select>
</div>
<div class="input-group inline-block">
<label for="per_lim[]">Program (min):</label>
<input type="number" name="per_lim[]" id="per_lim[]" min="1" step="1" required>
</div>
<div class="input-group inline-block">
<label for="limASR[]">Lim. ASR (%):</label>
<input type="number" min="0" max="100" step="0.1" name="limASR[]" id="limASR[]" required>
</div>
<div class="input-group inline-block">
<label for="limACD[]">Lim. ACD (Segs):</label>
<input type="number" min="0" name="limACD[]" id="limACD[]" required>
</div>
LRDTAB;

$type = isset($_GET['type']) ? $_GET['type'] : 'PER';


function tableRot($row, $groups_arr)
{
	global $trunks;
	$salida = '<table class="tprog">
		<thead>
			<tr>
				<th>Gid</th>
				<th>Group</th>
				<th>Time (min)</th>
				<th>Trunk</th>
				<th>ASR</th>
				<th>ACD</th>
			</tr>
		</thead>
		<tbody>';
	$group = explode('|', $row['group']);
	$prog = explode('|', $row['prog']);
	$trunk = explode('|', $row['trunk']);
	$asr = explode('|', $row['asr']);
	$acd = explode('|', $row['acd']);

	foreach ($prog as $i => $g) {
		$mitrunk = dato_from_id($trunk[$i], $trunks, 'providerip');
		$salida .= "<tr>
		<td>{$group[$i]}</td>
		<td>{$groups_arr[$group[$i]]['sim_team_name']}</td>
		<td>{$prog[$i]}</td>
		<td>{$mitrunk}</td>
		<td>{$asr[$i]}</td>
		<td>{$acd[$i]}</td>
		</tr>";
	}
	$salida .= '</tbody></table>';
	return $salida;
}

$conn = new mysqli($dbhost, $dbuser, $dbpw, $dbname);
if ($conn->connect_error) {
	die("Conexión fallida: " . $conn->connect_error);
}

$rotations = [];
if ($result_rotations = $conn->query('SELECT id, name, type, `group`, 
	prog,trunk_group_id, trunk,asr,acd, algorithm, active, time_activation,
	 time_inactivation FROM rotations')) {
	while ($row = $result_rotations->fetch_assoc()) {
		$rotations[] = $row;
	}
}
$result = $conn->query('SELECT sim_team_id, sim_team_name FROM sim_team ORDER BY sim_team_name ASC');

$groups_opt = '';
$groups_arr = [];
if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		$groups_arr[$row['sim_team_id']] = $row;
		$groups_opt .= "<option value='{$row['sim_team_name']} ({$row['sim_team_id']})'>{$row['sim_team_name']}</option>";
	}
}

// Cerrar conexión
//$conn->close();
$message = isset($_REQUEST['message']) ? $_REQUEST['message'] : 'Welcome!';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
	<link rel="manifest" href="../favicon/site.webmanifest">
	<link rel="mask-icon" href="../favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
	<link href="../style.css" rel="stylesheet" type="text/css">
	<link href="./add_rotation.css?<?= filemtime('./add_rotation.css') ?>" rel="stylesheet" type="text/css">
	<title>Group Template</title>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
</head>

<body>
	<script>
		var groups_opt = "<?= $groups_opt ?>";
		var trunks_opt = "<?= $trunks_opt ?>";
	</script>
	<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="border">
		<tr class="topbg">
			<td height="22" colspan="2" align="center"><strong>Group Scheduler</strong></td>
		</tr>
		<tr class="tdbg">
			<td><strong>Navigation:</strong>
				<a href="scheduler.php" target=main>List</a>
				&nbsp;|&nbsp;
				<a href="scheduler.php?action=add" target=main>Add</a>
			</td>
			<td><strong>Add Rotation:</strong>
				<a href="add_rotation.php?type=TIME" target=main>By Time</a>
				&nbsp;|&nbsp;
				<a href="add_rotation.php?type=PER" target=main>By Period</a>
				&nbsp;|&nbsp;
				<a href="add_rotation.php?type=LIM" target=main>By Limit</a>
			</td>
		</tr>
	</table>
	<section id="registration-page">
		<form action="add_rotation_proc.php?action=add" method="post" name="myform" class="signup-form">
			<input type="hidden" name="type" value="<?= $type ?>">
			<div class="message"><?= $message ?></div>
			<div class="form-header topbg">
				<h3> --- <?= $tipo[$type] ?> ---</h3>
			</div>
			<div class="form-body">
				<div class="row">
					<div class="input-group">
						<label for="name">Name:</label>
						<input type="text" name="name" id="name" autocomplete="false" required>
					</div>
				</div>
				<div class="row">
					<div class="input-group">
						<label for="hour_init">Init Hour:</label>
						<input type="time" name="hour_init" id="hour_init" required class="time-pickable" autocomplete="false">
					</div>
					<div class="input-group">
						<label for="hour_end">End Hour:</label>
						<input type="time" name="hour_end" id="hour_end" required class="time-pickable" autocomplete="false">
					</div>
					<?php if ($type == 'LIM') : ?>
						<div class="input-group">
							<label for="trunk_group">Trunk Group:</label>
							<select name="trunk_group" id="trunk_group">
								<?php foreach ($trunk_groups as $group) : ?>
									<option value="<?= $group['id'] ?>"><?= $group['name'] ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="input-group">
							<label for="algorithm">Policy:</label>
							<select name="algorithm" id="algorithm">
								<option value="ASR">ASR</option>
								<option value="ACD">ACD</option>
								<option value="ASR and ACD">ASR and ACD</option>
								<option value="ASR or ACD">ASR or ACD</option>
							</select>
						</div>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="input-group topbg">
						<!-- input type="button" value="Add Group to Circuit" disabled -->
						<h3>Cycle</h3>
					</div>
				</div>
				<div id="fin"></div>
			</div>
			<div class="input-group">
				<input type="button" value="+" style="width: 3em;" onclick="addGroup(groups_opt,trunks_opt,'<?= $type ?>');">
			</div>
			<div class="form-footer">
				<input type="submit" name="submit" id="submit" value="Save">
				<input type="reset" name="cancel" Id="Cancel" value="Cancel">
			</div>
		</form>
	</section>

	<!-- Grupos activos -->

	<section id="table">
		<table class="t_display">
			<thead>
				<tr>
					<!-- <th>ID</th> -->
					<th>Name</th>
					<th>Type</th>
					<th>Group Prog.</th>
					<th>Trunk Group</th>
					<th>Algorithm</th>
					<th>Activation</th>
					<th>Deactivation</th>
					<th>Active</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($rotations as  $rot) : ?>
					<tr>
						<!-- <td><?= $rot['id'] ?></td> -->
						<td><?= $rot['name'] ?></td>
						<td><?= $rot['type'] ?></td>
						<td><?= tableRot($rot, $groups_arr); ?></td>
						<td><?= dato_from_id($rot['trunk_group_id'], $trunk_groups, 'name') ?>
						</td>
						<td><?= $rot['algorithm'] ?></td>
						<td><?= $rot['time_activation'] ?></td>
						<td><?= $rot['time_inactivation'] ?></td>
						<td>
							<a href="add_rotation_proc.php?action=change_status&id=<?= $rot['id'] ?>" class="a-btn a-btn-<?= $rot['active'] ?>">
								<?= $rot['active'] ?>
							</a>
						</td>
						<td>
							<a href="add_rotation_proc.php?action=delete&id=<?= $rot['id'] ?>">Delete</a>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</section>
	<script src="../dynamic.js"></script>
	<script src="../check.js"></script>
	<script src="./add_rotation.js?<?= filemtime('./add_rotation.js') ?>"></script>
	<script>
		function eliminarRegistro(id) {
			// Aquí puedes escribir el código JavaScript para eliminar el registro con el ID proporcionado
		}

		function cambiarEstado(id) {
			// Aquí puedes escribir el código JavaScript para cambiar el estado del registro con el ID proporcionado
		}
	</script>
</body>

</html>