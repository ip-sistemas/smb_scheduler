<?php
if (php_sapi_name() == 'cli') {
	define('INI','');
	define('FIN','');
} else {
	define('INI','<pre>');
	define('FIN','</pre>');
}

function v()
{
	$resto = func_get_args();
	echo INI;
	if (count($resto)==1) {
		myprint_r(array_shift($resto));
	} else {
		myprint_r($resto);
	}
	echo FIN, PHP_EOL;
}

function vq()
{
	$resto = func_get_args();
	if (count($resto)==1) {
		v(array_shift($resto));
	} else {
		v($resto);
	}
	exit("\n---***---\n");
}

function myprint_r($my_array)
{
	static $i = -1;
	$espacios = '|  ';
	$espaciox = '+———';
	$sep = '————————————————';
	if (is_array($my_array)) {
		$i++;
		$z = str_repeat($espacios, $i).$espaciox;
		echo PHP_EOL;
		echo $z, $sep, PHP_EOL;
		foreach ($my_array as $k => $v) {
			echo str_repeat($espacios, $i), "[{$k}] = ";
			myprint_r($v);
			echo "\n";
		}
		echo $z, $sep;
		$i--;
		return;
	}
	echo $my_array;
}
