function addGroup(groups_opt, trunks_opt, type) {
	var grupos = `
<div class="input-group">
	<label for="group[]">Group:</label>
	<select name="group[]" id="group[]">${groups_opt}</select>
</div>
`;

	var btnDelete = `
<div class="input-group">
<input type="button" value="-" style="width: 3em;" onclick="delGroup(this);">
</div>
`;

	var template = new Array();
	template["TIME"] = `
<div class="input-group">
	<label for="prog[]">Program:</label>
	<input default="00:00" type="time" name="prog[]" id="prog[]" class="time-pickable" autocomplete="false" required>
</div>
`;

	template["PER"] = `
<div class="input-group">
<label for="prog">Program (min):</label>
<input type="number" name="prog[]" id="prog[]" min="1" step="1" required>
</div>
`;

	template["LIM"] = `
<div class="input-group inline-block">
<label for="trunk[]">Trunk:</label>
<select name="trunk[]" id="trunk[]">
${trunks_opt}
</select>
</div>
<div class="input-group inline-block">
<label for="prog[]">Program (min):</label>
<input type="number" name="prog[]" id="prog[]" min="1" step="1" required>
</div>
<div class="input-group inline-block">
<label for="limASR[]">Lim. ASR (%):</label>
<input type="number" min="0" max="100" step="0.1" name="limASR[]" id="limASR[]" required>
</div>
<div class="input-group inline-block">
<label for="limACD[]">Lim. ACD (Segs):</label>
<input type="number" min="0" name="limACD[]" id="limACD[]" required>
</div>
`;

	var divIzq = '<div class="row">';
	var divDer = "</div>";

	d = document.getElementById("fin");
	d.outerHTML =
		divIzq + grupos + template[type] + btnDelete + divDer + d.outerHTML;
	document.getElementById("submit").disabled = false;
	activate();
}

function delGroup(linea) {
	var divPadre = linea.parentElement;
	divPadre.parentElement.remove();
}

function setType(option) {
	var divs_plus = document.querySelectorAll(".div_plus");
	var divs_per = document.querySelectorAll(".div_per");
	var divs_time = document.querySelectorAll(".div_time");
	var divs_lim = document.querySelectorAll(".div_lim");

	divs_per.forEach((el) => {
		var elinput = el.querySelector("input,select");
		if (elinput.hasAttribute("required"))
			elinput.removeAttribute("required");
		el.style.display = "none";
	});

	divs_time.forEach((el) => {
		var elinput = el.querySelector("input,select");
		if (elinput.hasAttribute("required"))
			elinput.removeAttribute("required");
		el.style.display = "none";
	});

	divs_lim.forEach((el) => {
		var elinput = el.querySelector("input,select");
		if (elinput.hasAttribute("required"))
			elinput.removeAttribute("required");
		el.style.display = "none";
	});

	switch (option) {
		case "PER":
			divs_per.forEach((el) => {
				var elinput = el.querySelector("input,select");
				if (elinput.hasAttribute("required"))
					elinput.setAttribute("required", "true");
				el.style.display = "flex";
			});
			break;
		case "TIME":
			divs_time.forEach((el) => {
				var elinput = el.querySelector("input,select");
				if (elinput.hasAttribute("required"))
					elinput.setAttribute("required", "true");
				el.style.display = "flex";
			});
			break;
		case "LIM":
			divs_lim.forEach((el) => {
				var elinput = el.querySelector("input,select");
				if (elinput.hasAttribute("required"))
					elinput.setAttribute("required", "true");
				el.style.display = "flex";
			});
			break;
	}

	console.log(divs_per, divs_time, divs_lim, option);

	divs_plus.forEach((el) => {
		el.style.display = "flex";
	});
}

function activate() {
	document.querySelectorAll(".time-pickable").forEach((timePickable) => {
		let activePicker = null;

		timePickable.addEventListener("focus", () => {
			if (activePicker) return;

			activePicker = show(timePickable);

			const onClickAway = ({ target }) => {
				if (
					target === activePicker ||
					target === timePickable ||
					activePicker.contains(target)
				) {
					return;
				}

				document.removeEventListener("mousedown", onClickAway);
				document.body.removeChild(activePicker);
				activePicker = null;
			};

			document.addEventListener("mousedown", onClickAway);
		});
	});
}

function show(timePickable) {
	const picker = buildPicker(timePickable);
	const { bottom: top, left } = timePickable.getBoundingClientRect();

	picker.style.top = `${top}px`;

	picker.style.left = `${left}px`;

	document.body.appendChild(picker);

	return picker;
}

function buildPicker(timePickable) {
	const picker = document.createElement("div");
	const hourOptions = [
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
		20, 21, 22, 23,
	].map(numberToOption);
	const minuteOptions = [
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
		20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
		38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
		56, 57, 58, 59,
	].map(numberToOption);

	picker.classList.add("time-picker");
	picker.innerHTML = `
		<select class="time-picker__select">
			${hourOptions.join("")}
		</select>
		:
		<select class="time-picker__select">
			${minuteOptions.join("")}
		</select>
	`;

	const selects = getSelectsFromPicker(picker);

	selects.hour.addEventListener(
		"change",
		() => (timePickable.value = getTimeStringFromPicker(picker)),
	);
	selects.minute.addEventListener(
		"change",
		() => (timePickable.value = getTimeStringFromPicker(picker)),
	);
	if (timePickable.value) {
		const { hour, minute } = getTimePartsFromPickable(timePickable);

		selects.hour.value = hour;
		selects.minute.value = minute;
	}

	return picker;
}

function getTimePartsFromPickable(timePickable) {
	const pattern = /^(\d+):(\d+)$/;
	const [hour, minute] = Array.from(timePickable.value.match(pattern)).splice(
		1,
	);

	return {
		hour,
		minute,
	};
}

function getSelectsFromPicker(timePicker) {
	const [hour, minute] = timePicker.querySelectorAll(".time-picker__select");

	return {
		hour,
		minute,
	};
}

function getTimeStringFromPicker(timePicker) {
	const selects = getSelectsFromPicker(timePicker);
	return `${selects.hour.value}:${selects.minute.value}`;
}

function numberToOption(number) {
	const padded = number.toString().padStart(2, "0");
	return `<option value="${padded}">${padded}</option>`;
}

activate();
