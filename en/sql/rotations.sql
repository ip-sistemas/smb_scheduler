CREATE TABLE IF NOT EXISTS rotations (
  id int(10) UNSIGNED NOT NULL,
  name varchar(50) NOT NULL,
  type enum('PER','TIME') NOT NULL,
  `group` varchar(512) NOT NULL,
  prog varchar(512) NOT NULL,
  active enum('Y','N') NOT NULL DEFAULT 'Y',
  time_activation time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;