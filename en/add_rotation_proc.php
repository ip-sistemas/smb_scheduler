<?php
require_once './debug.php';

if (!isset($_REQUEST['action'])) {
	header('Location: add_rotation.php');
}

require_once '../inc/config.inc.php';
$mysqli = new mysqli($dbhost, $dbuser, $dbpw, $dbname);
if (mysqli_connect_errno()) {
	printf("Error de conexión: %s\n", mysqli_connect_error());
	exit();
}

switch ($_REQUEST['action']) {
	case 'add':

	if($_REQUEST['type']=='LIM'){
		$temp = array_reverse($_REQUEST['prog']);
		$temp[0] = -1;
		$_REQUEST['prog']=array_reverse($temp);
		$temp = array_reverse($_REQUEST['limASR']);
		$temp[0] = -1;
		$_REQUEST['limASR']=array_reverse($temp);
		$temp = array_reverse($_REQUEST['limACD']);
		$temp[0] = -1;
		$_REQUEST['limACD']=array_reverse($temp);
	}
		foreach ($_REQUEST['group'] as &$group) {
			$parts = explode("(", $group);
			if (count($parts) > 1) {
				$group = (int) rtrim($parts[1], ")");
			}
		}

		!isset($_REQUEST['algorithm']) && $_REQUEST['algorithm'] = '';
		!isset($_REQUEST['trunk_group']) && $_REQUEST['trunk_group'] = 0;
		!isset($_REQUEST['trunk'])       && $_REQUEST['trunk'] = [];
		!isset($_REQUEST['limACD'])      && $_REQUEST['limASR'] = [];
		!isset($_REQUEST['limACD'])      && $_REQUEST['limACD'] = [];


		$stmt = $mysqli->prepare('INSERT INTO rotations(name, type, `group`, prog, trunk_group_id, trunk, asr, acd, active, algorithm, time_activation, time_inactivation)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
		$stmt->bind_param('ssssdsssssss', $name, $type, $group, $prog, $trunk_group_id, $trunk, $asr, $acd, $active, $algorithm, $hour_init, $hour_end);

		$name = $_REQUEST['name'];
		$type = $_REQUEST['type'];
		$hour_init = $_REQUEST['hour_init'];
		$hour_end = $_REQUEST['hour_end'];
		$algorithm = $_REQUEST['algorithm'];
		$active = 'N';
		$trunk_group_id = $_REQUEST['trunk_group'];
		$group = join('|', $_REQUEST['group']);
		$prog = join('|', $_REQUEST['prog']);
		$trunk = join('|', $_REQUEST['trunk']);
		$asr = join('|', $_REQUEST['limASR']);
		$acd = join('|', $_REQUEST['limACD']);

		if ($stmt->execute() === true) {
			$id = $stmt->insert_id;
			if ($type =='LIM') {
				$zdir = __DIR__ . "/cron/txt/group_{$id}.txt";	
			}else{	
				$zdir = __DIR__ . "/cron/txt/{$id}.txt";
			}
			touch($zdir);
			$message = "Agregado Correctamente con id = $id.";
		} else {
			$message =  'Error al insertar el registro: ' . $stmt->error;
		}
		break;

	case 'delete':
		$stmt = $mysqli->prepare('DELETE FROM rotations WHERE id = ?');
		$stmt->bind_param("i", $_REQUEST['id']);
		if ($stmt->execute() === true) {
			$message = 'Eliminado correctamente el registro ' . $_REQUEST['id'];
			unlink(__DIR__ . "/cron/txt/{$_REQUEST['id']}.txt");
		} else {
			$message =  'Error al tratar de eliminar el registro ' . $_REQUEST['id'] . '. Error SQL:' . $stmt->error;
		}

		break;
	case 'change_status':
		$stmt = $mysqli->prepare("UPDATE rotations SET active = IF(active='Y', 'N', 'Y') WHERE id = ?");
		$stmt->bind_param("i", $_REQUEST['id']);
		if ($stmt->execute() === true) {
			if ($active = $mysqli->query("SELECT active FROM rotations WHERE id = {$_REQUEST['id']} LIMIT 1")) {
				if ($active->fetch_row()[0] == 'N') {
					unlink(__DIR__ . "/cron/txt/{$_REQUEST['id']}.txt");
				}
			}
			$message = 'Modificado correctamente el registro ' . $_REQUEST['id'];
		} else {
			$message =  'Error al tratar de modificar el registro ' . $_REQUEST['id'] . '. Error SQL:' . $stmt->error;
		}
		break;
}
// Cerrar la conexión
$mysqli->close();
header('Location: add_rotation.php?m=' . $message);
