<?php
/**
Realizado por: Arnoldo Bric (arnoldobr@gmail.com) 
para 3valtech.inc
Versión 0.1
2023-09-05
*/

define("OK", true);
session_start();

require_once 'debug.php';

function second_to_time($segundos)
{
    $horas = floor($segundos / 3600);
    $minutos = floor(($segundos % 3600) / 60);
    $segundosRestantes = $segundos % 60;
    // Formatear los valores como cadena
    $cadenaFormateada = sprintf("%02d:%02d:%02d", $horas, $minutos, $segundosRestantes);
    return $cadenaFormateada;
}

function my_cmp($a, $b)
{
	global $order_type;
	global $order_key;

	//echo "$a[name] $b[name] $a[$order_key] $b[$order_key]<br>";
	//if (empty($a[$order_key]) && empty($b[$order_key])) return 0;
	//if(empty($b[$order_key])) return -1;
	if ($a[$order_key] == $b[$order_key]) return 0;
	else if ($order_type == "desc") return ($a[$order_key] < $b[$order_key]) ? 1 : -1;
	else return ($a[$order_key] > $b[$order_key]) ? 1 : -1;
}

if (!isset($_SESSION['usertype'])) {
	require_once('login.php');
	exit;
}

require_once("global.php");
// require_once("excel_class.php");

if (!$_REQUEST['order']) {
	$_REQUEST['order'] = "asc";
}

if ($_REQUEST['order'] == "desc"){
	$order_type2 = 'asc';
} 
else{
	$order_type2 = 'desc';
} 

if (!$_REQUEST['order_key']) {
	$_REQUEST['order_key'] = "name";
}

$order_type = $_REQUEST['order'];
$order_key = $_REQUEST['order_key'];

$start_time = $_REQUEST['start_time'];
if (!$start_time) $start_time = date("Y-m-d");

$end_time = $_REQUEST['end_time'];
if (!$end_time) $end_time = date("Y-m-d");

$menorFecha = new DateTime($start_time);
$mayorFecha = new DateTime($end_time);

// Compara las fechas para ordenarlas adecuadamente
if ($menorFecha > $mayorFecha) {
	$temp = $menorFecha;
	$menorFecha = $mayorFecha;
	$mayorFecha = $temp;
}

// Calcula la diferencia en días entre las dos fechas
$diferencia = $menorFecha->diff($mayorFecha);
$diasDiferencia = $diferencia->days;

// Inicializa la fecha actual con la fecha mayor
$fechaActual = clone $menorFecha;

//$fechas = [];
// Itera mientras la fecha actual sea mayor o igual a la fecha menor
//while ($fechaActual >= $menorFecha) {
	//$fechas[] = $fechaActual->format('Y-m-d');
	// Resta un día a la fecha actual
	//$fechaActual->sub(new DateInterval('P1D'));
//}

$fechas = [];
// Itera mientras la fecha actual sea menor o igual a la fecha mayor
while ($fechaActual <= $mayorFecha) {
    $fechas[] = $fechaActual->format('Y-m-d');
    // Suma un día a la fecha actual
    $fechaActual->add(new DateInterval('P1D'));
}
 
$ch = "selected";
$name_ch = "All";

$totales = [];
foreach ($fechas  as $mifecha) {
	$totales[$mifecha] = ['asr'=>0,'acd'=>0,'acd_s'=>0,'ct'=>0,'cc'=>0, 'tcount'=>0, 'cc_over_asr'=>0,'tot_acd' => 0,];
	$sql = "SELECT sim_name as name,sum(duration) as calltime,count(id) as callcount 
		from call_record 
		where dir = 0 and duration > 0 and DATE(time) = '$mifecha'  
		group by name
		order by name ASC";

	$query = $db->query($sql);
	while ($row = $db->fetch_array($query)) {
		$calltime += $row[1];
		$callcount += $row[2];
		$row['acd'] = round($row[1] / $row[2]);
		$row['acd_s'] = second_to_time($row['acd']);;
		$row['calltime_s'] = second_to_time($row[1]);
		$rsdb[$row['name']]['f'][$mifecha] = $row; //lrdtab
	}
	
	$calltime_s = second_to_time($calltime);
	if ($callcount) $acd = round($calltime / $callcount);
	$acd_s = second_to_time($acd);
	$sql = "SELECT sim_name as name, count(id) 
		from call_record 
		where dir=0 and duration>=0 and DATE(time)='$mifecha' 
		group by sim_name
		order by sim_name ASC";

	$query = $db->query($sql);
	while ($row = $db->fetch_array($query)) {
		$rsdb[$row['name']]['f'][$mifecha]['asr'] = 
			(round($rsdb[$row['name']]['f'][$mifecha]['callcount'] / $row[1], 3) * 100) . "%";
		$rsdb[$row['name']]['f'][$mifecha]['tcount'] = $row[1];
		$tcount += $row[1];
	}

}

foreach ($rsdb as $sim => $data) {

	foreach ($data['f'] as $f=>$d) {
		$totales[$f]['ct'] += $d['calltime'];
		$totales[$f]['acd'] += $d['acd'];
		$d['acd'] > 0 && $totales[$f]['tot_acd'] ++;
		$totales[$f]['cc'] += $d['callcount'];
		$totales[$f]['tcount'] += $d['tcount'];
		$totales[$f]['cc_over_asr'] += $d['callcount']/$d['asr'];
	}
}


if ($tcount) $asr = (round($callcount / $tcount, 3) * 100) . "%";

//echo "asr:".round($rs[1]/$rs1[0],3);
//print_r($rsdb);

if ($order_key) uasort($rsdb, "my_cmp"); else{
}

ksort($rsdb);

if ($_REQUEST['submit_value'] == 'Export') {

	if ($name == 0) $filename = "CDR_" . $type . "_ALL($start_time~$end_time).xls";
	else $filename = "CDR_" . $type . $name . "($start_time~$end_time).xls";

	$return[0][0] = $type;
	$return[0][1] = "ASR";
	$return[0][2] = "ACD";
	$return[0][3] = "Call Time";
	$return[0][4] = "Connected Count";
	//$return[0][5]="All Call Count";

	$i = 1;
	foreach ($rsdb as $rs) {
		$return[$i][0] = "" . $rs['name'];
		$return[$i][1] = "" . $rs['asr'];
		$return[$i][2] = "" . $rs['acd_s'];
		$return[$i][3] = "" . $rs['calltime_s'];
		$return[$i][4] = "" . $rs['callcount'];
		//$return[$i][5]=($rs['asr'])?round((float)$rs['callcount']/$rs['asr']):0;
		$i++;
	}

	//Create_Excel_File($filename,$return);
	exit;
}

$sumct=[];
$totalsumct=0;
foreach ($rsdb as $k => $f) {
	$t=0;
	foreach ($f['f'] as $mifecha) {
		$t+=$mifecha['calltime'];
	}
	$sumct[$k]=['segs' => $t,'segs_h'=> second_to_time($t)];
	$totalsumct+=$t;
}
$totalsumct_h=second_to_time($totalsumct);
//vq($sumct, $rsdb);
require_once('stats.html');
