<?php
// Función de prueba básica
function test($description, $callback) {
    try {
        $callback();
        echo "✔️ Aprobado: $description\n";
    } catch (Exception $e) {
        echo "❌ Falla: $description\n";
        echo "  Error: " . $e->getMessage() . "\n";
    }
}

// Función de aserción
function assertEqual($expected, $actual) {
    if ($expected != $actual) {
        throw new Exception("Esperado '$expected' pero Obtenido '$actual'");
    }
}
