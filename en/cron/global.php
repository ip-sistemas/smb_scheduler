<?php

date_default_timezone_set('America/Caracas');
$perpage = '64';
require_once('conn.inc.php');
require_once('sqlsend.php');

$URL = "";
if (isset($_SERVER['HTTP_REFERER'])) {
    $URL = $_SERVER['HTTP_REFERER'];
}

function sendto_xchanged($send)
{
    global $phpsvrport;
    if (!$send) {
        return;
    }
    $flag = 0;
    if (($socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) <= 0) {
        echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n";
        //exit;
    }
    foreach ($send as $sendbuf) {
        if (socket_sendto($socket, $sendbuf, strlen($sendbuf), 0, "127.0.0.1", $phpsvrport) === false) {
            echo("sendto error");
            //exit;
        }
    }
    echo "Mydify Success";
}

function multi($count, $page, $numofpage, $url)
{
    if ($numofpage <= 1) {
        return;
    } else {
        $fengye = "<a href=\"{$url}&page=1\"><< </a>";
        $flag = 0;
        for ($i = $page - 3; $i <= $page - 1; $i++) {
            if ($i < 1) {
                continue;
            }
            $fengye .= " <a href={$url}&page=$i>&nbsp;$i&nbsp;</a>";
        }
        $fengye .= "&nbsp;&nbsp;<b>$page</b>&nbsp;";
        if ($page < $numofpage) {
            for ($i = $page + 1; $i <= $numofpage; $i++) {
                $fengye .= " <a href={$url}&page=$i>&nbsp;$i&nbsp;</a>";
                $flag++;
                if ($flag == 4) {
                    break;
                }
            }
        }
        $fengye .= " <input type='text' size='2' style='height: 16px; border:1px solId #E7E3E7' onkeydown=\"javascript: if(event.keyCode==13) location='{$url}&page='+this.value;\"> <a href=\"{$url}&page=$numofpage\"> >></a> &nbsp;(共 $numofpage 頁)";
        return $fengye;
    }
}

function showpage($sfilename, $CurrentPage, $totalnumber, $maxperpage, $ShowTotal, $ShowAllPages, $strUnit)
{
    if ($totalnumber % $maxperpage == 0) {
        $n = $totalnumber / $maxperpage;
    } else {
        $n = (int)($totalnumber / $maxperpage) + 1;
    }

    $strTemp = "<table align='center'><tr><td>";
    if ($ShowTotal == true) {
        $strTemp = $strTemp . "Total <b>" . $totalnumber . "</b> " . $strUnit . " &nbsp;&nbsp;";
    }
    if ($CurrentPage < 2) {
        $strTemp = $strTemp . "index backward&nbsp;";
    } else {
        $strTemp = $strTemp . "<a href='" . $sfilename . "page=1'>index</a>&nbsp;";
        $strTemp = $strTemp . "<a href='" . $sfilename . "page=" . ($CurrentPage - 1) . "'>backward</a>&nbsp;";
    }

    if ($n - $CurrentPage < 1) {
        $strTemp = $strTemp . "forward end";
    } else {
        $strTemp = $strTemp . "<a href='" . $sfilename . "page=" . ($CurrentPage + 1) . "'>forward</a>&nbsp;";
        $strTemp = $strTemp . "<a href='" . $sfilename . "page=" . $n . "'>end</a>";
    }

    $strTemp = $strTemp . " &nbsp;pages:<strong><font color=red>" . $CurrentPage . "</font>/" . $n . "</strong>page ";
    $strTemp = $strTemp . " &nbsp;<b>" . $maxperpage . "</b>" . $strUnit . "/page";

    if ($ShowAllPages = true) {
        $strTemp = $strTemp . " &nbsp;goto:<select name='page' size='1' onchange=javascript:window.location='" . $sfilename . "page=" . "'+this.options[this.selectedIndex].value;>";
        $c_page = 500;
        $i = $CurrentPage - $c_page;
        if ($i < 1) {
            $i = 1;
        }
        if ($n > $CurrentPage + $c_page) {
            $n = $CurrentPage + $c_page;
        }
        for ($i = 1; $i <= $n; $i++) {
            $strTemp = $strTemp . "<option value='" . $i . "'";
            if ((int)($CurrentPage) == (int)($i)) {
                $strTemp = $strTemp . " selected ";
            }
            $strTemp = $strTemp . ">The" . $i . "page</option>";
        }
        $strTemp = $strTemp . "</select>";
    }
    $strTemp = $strTemp . "</td></tr></table>";
    return $strTemp;
}

function showpage2($sfilename, $CurrentPage, $totalnumber, $maxperpage, $ShowTotal, $ShowAllPages, $strUnit, $form, $post)
{
    if ($totalnumber % $maxperpage == 0) {
        $n = $totalnumber / $maxperpage;
    } else {
        $n = (int)($totalnumber / $maxperpage) + 1;
    }

    $strTemp = "<table align='center'><tr><td>";
    if ($ShowTotal == true) {
        $strTemp = $strTemp . "Total <b>" . $totalnumber . "</b> " . $strUnit . " &nbsp;&nbsp;";
    }
    if ($CurrentPage < 2) {
        $strTemp = $strTemp . "index backward&nbsp;";
    } else {
        $strTemp = $strTemp . "<span class='spanpage' onclick='return changepage(\"" . $sfilename . "page=1\", " . $form . ")'>index</span>&nbsp;";
        $strTemp = $strTemp . "<span class='spanpage' onclick='return changepage(\"" . $sfilename . "page=" . ($CurrentPage - 1) . "\", " . $form . ")'>backward</span>&nbsp;";
    }

    if ($n - $CurrentPage < 1) {
        $strTemp = $strTemp . "forward end";
    } else {
        $strTemp = $strTemp . "<span class='spanpage' onclick='return changepage(\"" . $sfilename . "page=" . ($CurrentPage + 1) . "\", " . $form . ")'>forward</span>&nbsp;";
        $strTemp = $strTemp . "<span class='spanpage' onclick='return changepage(\"" . $sfilename . "page=" . $n . "\", " . $form . ")'>end</span>&nbsp;";
    }

    $strTemp = $strTemp . " &nbsp;pages：<strong><font color=red>" . $CurrentPage . "</font>/" . $n . "</strong>page ";
    $strTemp = $strTemp . " &nbsp;<b>" . $maxperpage . "</b>" . $strUnit . "/page";

    if ($ShowAllPages = true) {
        $strTemp = $strTemp . " &nbsp;goto：<select name='page' size='1' onchange='return selectchangepage(\"" . $sfilename . "\",this.options[this.selectedIndex].value, " . $form . ")'>";
        for ($i = 1; $i <= $n; $i++) {
            $strTemp = $strTemp . "<option value='" . $i . "'";
            if ((int)($CurrentPage) == (int)($i)) {
                $strTemp = $strTemp . " selected ";
            }
            $strTemp = $strTemp . ">The" . $i . "page</option>";
        }
        $strTemp = $strTemp . "</select>";
    }
    $strTemp = $strTemp . "</td></tr></table>";
    return $strTemp;
}



function template($template)
{
    return "../template/admin/$template.htm";
}

function WriteErrMsg($ErrMsg1)
{
    $strErr = "Wrong message. Reasons:`\n $ErrMsg1\n";
    echo $strErr;
    //exit;
}


function WriteSuccessMsg($SuccessMsg, $URL)
{
    $strErr = "$SuccessMsg\n$URL\n";
    echo $strErr;
    //exit;//LRDTAB
}

function export_sim($where, $orderby)
{
    global $db;
    $query = $db->query("SELECT sim.* FROM sim  $where $orderby");
    //echo "SELECT sim.* FROM sim  $where $orderby";
    $filename = "SIM_" . date("YmdHim") . ".xls";

    $return[0][0] = "Slot ID";
    $return[0][1] = "ONLINE";
    $return[0][2] = "IMSI";
    $return[0][3] = "ICCID";
    $return[0][4] = "NUMBER";

    $i = 1;
    while ($rs = $db->fetch_array($query)) {
        if ($rs['sim_login'] == 0 || $rs['sim_login'] == 12) {
            $rs['alive'] = 0;
        } else {
            $rs['alive'] = 1;
        }
        $rs['imsi'] = str_replace('"', '', $rs['imsi']);
        $rs['iccid'] = str_replace(' ', '', $rs['iccid']);
        $return[$i][0] = "" . $rs['sim_name'];
        $return[$i][1] = "" . $rs['alive'];
        $return[$i][2] = "" . $rs['imsi'];
        $return[$i][3] = "" . $rs['iccid'];
        $return[$i][4] = "" . $rs['simnum'];
        $i++;
    }
    //	Create_Excel_File($filename, $return);
    //exit;
}


if (!function_exists('v')) {
    function v()
    {
        $resto = func_get_args();

        if (php_sapi_name() == 'cli') {
            define('INI', '');
            define('FIN', '');
        } else {
            define('INI', '<pre>');
            define('FIN', '</pre>');
        }

        echo INI;

        if (count($resto) == 1) {
            myprint_r(array_shift($resto));
        } else {
            myprint_r($resto);
        }

        echo FIN, PHP_EOL;
    }



    function vq()
    {
        $resto = func_get_args();

        if (count($resto) == 1) {
            v(array_shift($resto));
        } else {
            v($resto);
        }
        exit("\n---***---\n");
    }

    function myprint_r_v1($my_array)
    {

        static $i = -1;
        $espacios = '|  ';
        $espaciox = '+———';
        $sep = '————————————————';
        if (is_array($my_array)) {
            $i++;
            $z = str_repeat($espacios, $i) . $espaciox;
            echo PHP_EOL;
            echo $z, $sep, PHP_EOL;
            foreach ($my_array as $k => $v) {
                echo str_repeat($espacios, $i), "| [{$k}] = ";
                myprint_r($v);
                echo "\n";
            }
            echo $z, $sep;
            $i--;
            return;
        }
        echo $my_array;
    }

    function myprint_r($my_array)
    {
        static $i = -1;
        $espacios = '|  ';
        $espaciox = '+———';
        $sep = '————————————————';

        if (is_array($my_array)) {
            $i++;
            $z = str_repeat($espacios, $i) . $espaciox;
            echo PHP_EOL;
            echo $z, $sep, PHP_EOL;
            foreach ($my_array as $k => $v) {
                echo str_repeat($espacios, $i), "| [{$k}] = ";
                myprint_r($v);
                echo "\n";
            }
            echo $z, $sep;
            $i--;
        } elseif (is_object($my_array)) {
            $i++;
            $z = str_repeat($espacios, $i) . $espaciox;
            echo PHP_EOL;
            echo $z, $sep, PHP_EOL;

            foreach ($my_array as $property => $value) {
                echo str_repeat($espacios, $i), "{$property} = ";
                myprint_r($value);
                echo "\n";
            }

            echo $z, $sep;
            $i--;
        } else {
            echo $my_array;
        }
    }
}
