<?php

/**
 * devuelve el id del sim_team actual para el sim dado
 * @param  string $simname nombre del sim (No es el id del sim)
 * @return string          Id del sim_team donde se encuentra actualmente el sim
 */
function bd_actual_sim_team($simname){
    global $dbhost, $dbuser, $dbpw, $dbname;
    $conn = new mysqli($dbhost, $dbuser, $dbpw, $dbname, 3306);
    $sim_team_actual = $conn->query("SELECT sim_team_id FROM sim WHERE sim_name = '{$simname}' LIMIT 1");
    $sim_team_actual = $sim_team_actual->fetch_assoc();
    return $sim_team_actual['sim_team_id'];
}