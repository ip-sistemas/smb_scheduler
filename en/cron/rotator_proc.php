<?php
require_once "global.php";
define("OK", true);
// session_start();
// if (!isset($_SESSION['usertype'])) {
// 	require_once 'login.php';
// 	exit;
// }

function gen_sim_where()
{
	if ($_REQUEST['chkAll0']) {
		$where = " where 1";
	} else if ($_REQUEST['sim_name']) {
		$where = " where sim_name in (" . $_REQUEST['sim_name'] . ") ";
	} else {
		$id = get_id();
		if (!$id) {
			WriteErrMsg("Not choose one SIM Slot!");
		}

		$where = " where sim_name in ($id)";
	}
	return $where;
}

function get_id()
{
	if ($_REQUEST['chkAll0']) {
		return "all SIM Slots";
	}

	$num = $_REQUEST['boxs'];
	$id = '';
	for ($i = 0; $i < $num; $i++) {
		if (!empty($_REQUEST["Id$i"])) {
			if ($id == "") {
				$id = $_REQUEST["Id$i"];
			} else {
				$id = $_REQUEST["Id$i"] . ",$id";
			}
		}
	}
	if ($_REQUEST['rstr']) {
		if ($id == "") {
			$id = $_REQUEST['rstr'];
		} else {
			$id = $_REQUEST['rstr'] . ",$id";
		}
	}
	return $id;
}

function my_cmp($a, $b)
{
	global $order_type;
	global $order_key;

	//echo "$a[name] $b[name] $a[$order_key] $b[$order_key]<br>";
	//if (empty($a[$order_key]) && empty($b[$order_key])) return 0;
	//if(empty($b[$order_key])) return -1;
	if ($a[$order_key] == $b[$order_key]) {
		return 0;
	} else if ($order_type == "desc") {
		return ($a[$order_key] < $b[$order_key]) ? 1 : -1;
	} else {
		return ($a[$order_key] > $b[$order_key]) ? 1 : -1;
	}
}

function second_to_time($t)
{
	$h = floor($t / 3600);
	$m = floor(($t - 3600 * $h) / 60);
	$s = $t - 3600 * $h - 60 * $m;
	$n = '';
	if ($h) {
		$n .= "{$h}h ";
	}

	if ($m) {
		$n .= "{$m}m ";
	}

	$n .= "{$s}s";
	return $n;
}

function get_rsdb()
{
	global $db;
	$_REQUEST = [];
	if (!$_REQUEST['order']) {
		$_REQUEST['order'] = "asc";
	}
	if ($_REQUEST['order'] == "desc") {
		$order_type2 = 'asc';
	} else {
		$order_type2 = 'desc';
	}

	if (!$_REQUEST['order_key']) {
		$_REQUEST['order_key'] = "name";
	}

	$order_type = $_REQUEST['order'];
	$order_key = $_REQUEST['order_key'];

	if ($_REQUEST['submit_value'] == '1 Hour') {
		$start_time = date("Y-m-d H:i", time() - 3600);
		$end_time = date("Y-m-d H:i");
	} else if ($_REQUEST['submit_value'] == '30 Min') {
		$start_time = date("Y-m-d H:i", time() - 1800);
		$end_time = date("Y-m-d H:i");
	} else {
		$start_time = $_REQUEST['start_time'];
		if (!$start_time) {
			$start_time = date("Y-m-d") . " 00:00";
		}

		$end_time = $_REQUEST['end_time'];
		if (!$end_time) {
			$end_time = date("Y-m-d H:i");
		}
	}

	$type = $_REQUEST['type'];
	if (!$type) {
		$type = "sim";
	}

	$name = $_REQUEST['name'];

	if (!$name) {
		$ch = "selected";
		$name_ch = "All";
	}
	if ($name) {
		$wh = "and " . $type . "_name=$name";
	}

	$select = "<select name=\"name\"  style=\"width:80px\" >\n\t<option value=\"0\" $ch>All</option>\n";
	if ($type == "line") {
		$query = $db->query("select id, " . $type . "_name as name from device_line where 1  order by line_name");
	} else {
		$query = $db->query("select id, " . $type . "_name as name from sim where 1 order by sim_name");
	}

	while ($row = $db->fetch_array($query)) {
		if ($name == $row['name']) {
			$row['ch'] = "selected";
			$name_ch = $row['name'];
			$channel[] = $row;
			$rsdb[$row['name']] = $row;
		} else if (!$name) {
			$channel[] = $row;
			$rsdb[$row['name']] = $row;
		}
		//$rsdb[$row['name']]=$row;
		$select .= "\t<option value=\"{$row['name']}\" $row[ch]>{$row['name']}</option>\n";
	}
	$select .= "</select>";

	$sql = "SELECT " . $type . "_name as name,sum(duration) as calltime,count(id) as callcount from call_record where dir=0 and duration>0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";
	$query = $db->query($sql);
	$calltime = 0;
	$callcount = 0;
	while ($row = $db->fetch_array($query)) {
		$calltime += $row[1];
		$callcount += $row[2];
		$row['acd'] = round($row[1] / $row[2]);
		$row['acd_s'] = second_to_time($row['acd']);
		$row['calltime_s'] = second_to_time($row[1]);
		$rsdb[$row['name']] = $row;
	}
	$calltime_s = second_to_time($calltime);
	if ($callcount) {
		$acd = round($calltime / $callcount);
	}

	$acd_s = second_to_time($acd);

	$sql = "SELECT " . $type . "_name as name,count(id) from call_record where dir=0 and duration>=0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";

	$query = $db->query($sql);
	$tcount = 0;
	while ($row = $db->fetch_array($query)) {
		$rsdb[$row['name']]['asr'] = (round($rsdb[$row['name']]['callcount'] / $row[1], 3) * 100) . "%";
		$rsdb[$row['name']]['tcount'] = $row[1];
		$tcount += $row[1];
	}

	if ($tcount) {
		$asr = (round($callcount / $tcount, 3) * 100) . "%";
	}

	if ($order_key) {
		uasort($rsdb, "my_cmp");
	}
	return $rsdb;
}
// =======================================================================

$_REQUEST['action'] = $_GET['action'] = 'savemoremodify';
$_REQUEST['bank_name'] = $_GET['bank_name'] = '';
$_REQUEST['group_id'] = $_GET['group_id'] = '';
$_REQUEST['order'] = $_GET['order'] = 'asc';
$_REQUEST['order_key'] = $_GET['order_key'] = 'sim_name';

$_REQUEST['auto_reset_remain'] = $_POST['auto_reset_remain'] = 0;
$_REQUEST['auto_reset_remain_s'] = $_POST['auto_reset_remain_s'] = 60;
$_REQUEST['cancel'] = $_POST['Cancel'] = 'Cancel';
$_REQUEST['chkall0'] = $_POST['chkAll0'] = '';
$_REQUEST['count_limit'] = $_POST['count_limit'] = '';
$_REQUEST['count_limit_no_connect'] = $_POST['count_limit_no_connect'] = 1;
$_REQUEST['dev_disable'] = $_POST['dev_disable'] = 0;
$_REQUEST['imei_mode'] = $_POST['imei_mode'] = 0;
$_REQUEST['limit_sms'] = $_POST['limit_sms'] = '';
$_REQUEST['month_limit_time'] = $_POST['month_limit_time'] = '';
$_REQUEST['month_reset_day'] = $_POST['month_reset_day'] = '';
$_REQUEST['no_answer_disable'] = $_POST['no_answer_disable'] = 1;
$_REQUEST['no_answer_limit'] = $_POST['no_answer_limit'] = '';
$_REQUEST['no_connected_limit'] = $_POST['no_connected_limit'] = '';
$_REQUEST['no_ring_disable'] = $_POST['no_ring_disable'] = 1;
$_REQUEST['no_ring_limit'] = $_POST['no_ring_limit'] = '';
$_REQUEST['short_call_disable'] = $_POST['short_call_disable'] = 1;
$_REQUEST['short_call_limit'] = $_POST['short_call_limit'] = '';
$_REQUEST['short_time'] = $_POST['short_time'] = '';
$_REQUEST['sim_name'] = $_POST['sim_name'] = $temp[3]; # Sims
$_REQUEST['Submit'] = $_POST['Submit'] = 'Save';
$_REQUEST['team_id'] = $_POST['team_id'] = $temp['2']; # Destiny team
$_REQUEST['team_id_modify'] = $_POST['team_id_modify'] = 1;
$_REQUEST['time_limit'] = $_POST['time_limit'] = '';
$_REQUEST['time_unit'] = $_POST['time_unit'] = '';

#==============================================================
v($_REQUEST);

$order_type = $_REQUEST['order'];
$order_key = $_REQUEST['order_key'];


$action = $_GET['action'];

$password = $_POST['Password'];
$name = $_POST['name'];
$team_id = $_POST['team_id'];
$id = $_GET['id'];
$dev_disable = $_POST['dev_disable'];
$sim_name = $_POST['sim_name'];
$imei = $_POST['imei'];
$time_unit = $_POST['time_unit'];
$imei_mode = $_POST['imei_mode'];
$where = gen_sim_where();

$remain_time = $_POST['remain_time'];
if ($remain_time == "" || $remain_time < -1) {
	$remain_time = -1;
}

if ($_POST['month_limit_time'] == "" || $_POST['month_limit_time'] < -1) {
	$_POST['month_limit_time'] = -1;
}

$month_remain_time = ($_POST['month_limit_time'] == -1) ? -1 : ($_POST['month_limit_time'] * 60);
if ($_POST['time_limit'] == "" || $_POST['time_limit'] < -1) {
	$_POST['time_limit'] = -1;
}

if ($_POST['limit_sms'] == "" || $_POST['limit_sms'] < -1) {
	$_POST['limit_sms'] = -1;
}

if ($_POST['no_ring_limit'] == "" || $_POST['no_ring_limit'] < -1) {
	$_POST['no_ring_limit'] = -1;
}

if ($_POST['no_answer_limit'] == "" || $_POST['no_answer_limit'] < -1) {
	$_POST['no_answer_limit'] = -1;
}

if ($_POST['short_call_limit'] == "" || $_POST['short_call_limit'] < -1) {
	$_POST['short_call_limit'] = -1;
}

if ($_POST['count_limit'] == "" || $_POST['count_limit'] < -1) {
	$_POST['count_limit'] = -1;
}

if ($_POST['no_connected_limit'] == "" || $_POST['no_connected_limit'] < -1) {
	$_POST['no_connected_limit'] = -1;
}

//echo $sim_name;
$ErrMsg = "";
if ($ErrMsg != "") {
	WriteErrMsg($ErrMsg);
} else {
	//$sim_rs=$db->fetch_array($db->query("SELECT * FROM sim where sim_name='$sim_name'"));
	//if($sim_rs['time_limit']!=$_POST[time_limit]) $t_remain=",remain_time=$_POST[time_limit]"; //重置时间
	$sql = "UPDATE sim SET ";
	if ($_POST['team_id_modify']) {
		$sql .= "sim_team_id='$team_id',";
	}

	if ($_POST['imei_mode_modify']) {
		$sql .= "imei_mode='$imei_mode',";
	}

	if ($_POST['month_limit_time_modify']) {
		$sql .= "month_limit_time='$_POST[month_limit_time]',month_remain_time='$month_remain_time',month_last_reset_time=now(),";
	}

	if ($_POST['month_reset_day_modify']) {
		$sql .= "month_reset_day='$_POST[month_reset_day]',";
	}

	if ($_POST['time_limit_modify']) {
		$sql .= "time_limit='$_POST[time_limit]',remain_time='$_POST[time_limit]',";
	}

	if ($_POST['count_limit_modify']) {
		$sql .= "count_limit='$_POST[count_limit]',count_remain=$_POST[count_limit],";
	}

	if ($_POST['limit_sms_modify']) {
		$sql .= "limit_sms='$_POST[limit_sms]',remain_sms='$_POST[limit_sms]',";
	}

	if ($_POST['count_limit_no_connect_modify']) {
		$sql .= "count_limit_no_connect='$_POST[count_limit_no_connect]',";
	}

	if ($_POST['no_connected_limit_modify']) {
		$sql .= "no_connected_limit='$_POST[no_connected_limit]',no_connected_remain=$_POST[no_connected_limit],";
	}

	if ($_POST['time_unit_modify']) {
		$sql .= "time_unit='$time_unit',";
	}

	if ($_POST['no_ring_modify']) {
		$sql .= "no_ring_limit='$_POST[no_ring_limit]',no_ring_disable='$_POST[no_ring_disable]',";
	}

	if ($_POST['no_answer_modify']) {
		$sql .= "no_answer_limit='$_POST[no_answer_limit]',no_answer_disable='$_POST[no_answer_disable]',";
	}

	if ($_POST['short_call_modify']) {
		$sql .= "short_call_limit='$_POST[short_call_limit]',short_call_disable='$_POST[short_call_disable]',";
	}

	if ($_POST['short_time_modify']) {
		$sql .= "short_time='$_POST[short_time]',";
	}

	if ($_POST['dev_disable_modify']) {
		$sql .= "dev_disable='$dev_disable',";
	}

	if ($_POST['auto_reset_remain_modify']) {
		$sql .= "auto_reset_remain='$_POST[auto_reset_remain]',";
	}

	if ($_POST['auto_reset_remain_s_modify']) {
		$sql .= "auto_reset_remain_s='$_POST[auto_reset_remain_s]',";
	}

	$sql .= "sim_login=sim_login $where";
	if ($_POST['dev_disable_modify'] && $dev_disable == 0) {
		$db->query("update sim SET no_ring_remain=0,no_answer_remain=0,short_call_remain=0 $where and dev_disable=1 and ((short_call_disable=1 and short_call_limit>0 and short_call_remain>=short_call_limit) or (no_ring_disable=1 and no_ring_limit>0 and no_ring_remain>=no_ring_limit) or (no_answer_disable=1 and no_answer_limit>0 and no_answer_remain>=no_answer_limit))");
	}

	if ($_POST['dev_disable_modify']) {
		$query = $db->query("select sim_name from sim $where and dev_disable!='$dev_disable'");
		while ($row = $db->fetch_array($query)) {
			if ($dev_disable == 0) {
				$db->query("insert into logs set sim_name='$row[sim_name]', log='enabled batch manually'");
			} else {
				$db->query("insert into logs set sim_name='$row[sim_name]', log='disabled batch manually'");
			}
		}
	};
	v($sql);
	$db->query($sql);

	$query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name $where");
	while ($row = $db->fetch_array($query)) {
		if ($_POST['month_reset_day_modify']) {
			$db->query("insert into logs set sim_name='$row[sim_name]', log='change month limit'");
		}

		sim_info($row, $send);
		if ($_POST['team_id_modify'] && $team_id) {
			$send[] = my_pack2(DEV_BINDING, $row['sim_name'], 0);
		}
	}

	sendto_xchanged($send);
	WriteSuccessMsg("<br><li>Modify SIM Slot successful!</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
}
