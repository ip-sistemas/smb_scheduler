<?php

require_once "global.php";

$_GET = [
    "action" => "savemodify",
    "bank_name" => "",
    "group_id" => "",
    "order" => "asc",
    "order_key" => "sim_name",
];

$_POST = [
    "team_id" => $temp[2],
    "old_team_id" => $temp[1],
    "sim_name" => $temp[3],
    "line_name" => 0,
    "imei_mode" => 0,
    "imei" => 000000000000000,
    "month_limit_time" => -1,
    "month_reset_day" => 1,
    "time_limit" => -1,
    "time_unit" => 1,
    "count_limit" => -1,
    "count_limit_no_connect" => 1,
    "limit_sms" => -1,
    "auto_reset_remain" => 0,
    "auto_reset_remain_s" => 60,
    "no_connected_limit" => -1,
    "no_ring_limit" => 9,
    "no_ring_disable" => 1,
    "no_answer_limit" => 7,
    "no_answer_disable" => 1,
    "short_call_limit" => 3,
    "short_call_disable" => 1,
    "short_time" => 8,
    "dev_disable" => 0,
    "oldname" => 8010,
    "old_disable" => 0,
    "chkAll0" => '',
    "Submit" => 'Save',
];

$_REQUEST = array_merge($_GET, $_POST);

define("OK", true);
//session_start();


//print_r($_POST);
if (!$_REQUEST['order']) {
    $_REQUEST['order'] = "asc";
}
if ($_REQUEST['order'] == "desc") {
    $order_type2 = 'asc';
} else {
    $order_type2 = 'desc';
}

if (!$_REQUEST['order_key']) {
    $_REQUEST['order_key'] = "sim_name";
}

$order_type = $_REQUEST['order'];
$order_key = $_REQUEST['order_key'];

function get_id()
{
    if ($_REQUEST['chkAll0']) {
        return "all SIM Slots";
    }

    $num = $_REQUEST['boxs'];
    $id = '';
    for ($i = 0; $i < $num; $i++) {
        if (!empty($_REQUEST["Id$i"])) {
            if ($id == "") {
                $id = $_REQUEST["Id$i"];
            } else {
                $id = $_REQUEST["Id$i"] . ",$id";
            }
        }
    }
    if ($_REQUEST['rstr']) {
        if ($id == "") {
            $id = $_REQUEST['rstr'];
        } else {
            $id = $_REQUEST['rstr'] . ",$id";
        }
    }
    return $id;
}

function gen_sim_where()
{
    if ($_REQUEST['chkAll0']) {
        $where = " where 1";
    } elseif ($_REQUEST['sim_name']) {
        $where = " where sim_name in (" . $_REQUEST['sim_name'] . ") ";
    } else {
        $id = get_id();
        if (!$id) {
            WriteErrMsg("Not choose one SIM Slot!");
        }

        $where = " where sim_name in ($id)";
    }
    return $where;
}

function my_cmp($a, $b)
{
    global $order_type;
    global $order_key;

    //echo "$a[name] $b[name] $a[$order_key] $b[$order_key]<br>";
    //if (empty($a[$order_key]) && empty($b[$order_key])) return 0;
    //if(empty($b[$order_key])) return -1;
    if ($a[$order_key] == $b[$order_key]) {
        return 0;
    } elseif ($order_type == "desc") {
        return ($a[$order_key] < $b[$order_key]) ? 1 : -1;
    } else {
        return ($a[$order_key] > $b[$order_key]) ? 1 : -1;
    }
}

function second_to_time($t)
{
    $h = floor($t / 3600);
    $m = floor(($t - 3600 * $h) / 60);
    $s = $t - 3600 * $h - 60 * $m;
    $n = '';
    if ($h) {
        $n .= "{$h}h ";
    }

    if ($m) {
        $n .= "{$m}m ";
    }

    $n .= "{$s}s";
    return $n;
}

function get_rsdb()
{
    global $db;
    $_REQUEST = [];
    if (!$_REQUEST['order']) {
        $_REQUEST['order'] = "asc";
    }
    if ($_REQUEST['order'] == "desc") {
        $order_type2 = 'asc';
    } else {
        $order_type2 = 'desc';
    }

    if (!$_REQUEST['order_key']) {
        $_REQUEST['order_key'] = "name";
    }

    $order_type = $_REQUEST['order'];
    $order_key = $_REQUEST['order_key'];

    if ($_REQUEST['submit_value'] == '1 Hour') {
        $start_time = date("Y-m-d H:i", time() - 3600);
        $end_time = date("Y-m-d H:i");
    } elseif ($_REQUEST['submit_value'] == '30 Min') {
        $start_time = date("Y-m-d H:i", time() - 1800);
        $end_time = date("Y-m-d H:i");
    } else {
        $start_time = $_REQUEST['start_time'];
        if (!$start_time) {
            $start_time = date("Y-m-d") . " 00:00";
        }

        $end_time = $_REQUEST['end_time'];
        if (!$end_time) {
            $end_time = date("Y-m-d H:i");
        }
    }

    $type = $_REQUEST['type'];
    if (!$type) {
        $type = "sim";
    }

    $name = $_REQUEST['name'];

    if (!$name) {
        $ch = "selected";
        $name_ch = "All";
    }
    if ($name) {
        $wh = "and " . $type . "_name=$name";
    }

    $select = "<select name=\"name\"  style=\"width:80px\" >\n\t<option value=\"0\" $ch>All</option>\n";
    if ($type == "line") {
        $query = $db->query("select id, " . $type . "_name as name from device_line where 1  order by line_name");
    } else {
        $query = $db->query("select id, " . $type . "_name as name from sim where 1 order by sim_name");
    }

    while ($row = $db->fetch_array($query)) {
        if ($name == $row['name']) {
            $row['ch'] = "selected";
            $name_ch = $row['name'];
            $channel[] = $row;
            $rsdb[$row['name']] = $row;
        } elseif (!$name) {
            $channel[] = $row;
            $rsdb[$row['name']] = $row;
        }
        //$rsdb[$row['name']]=$row;
        $select .= "\t<option value=\"{$row['name']}\" $row[ch]>{$row['name']}</option>\n";
    }
    $select .= "</select>";

    $sql = "SELECT " . $type . "_name as name,sum(duration) as calltime,count(id) as callcount from call_record where dir=0 and duration>0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";
    $query = $db->query($sql);
    $calltime = 0;
    $callcount = 0;
    while ($row = $db->fetch_array($query)) {
        $calltime += $row[1];
        $callcount += $row[2];
        $row['acd'] = round($row[1] / $row[2]);
        $row['acd_s'] = second_to_time($row['acd']);
        $row['calltime_s'] = second_to_time($row[1]);
        $rsdb[$row['name']] = $row;
    }
    $calltime_s = second_to_time($calltime);
    if ($callcount) {
        $acd = round($calltime / $callcount);
    }

    $acd_s = second_to_time($acd);

    $sql = "SELECT " . $type . "_name as name,count(id) from call_record where dir=0 and duration>=0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";

    $query = $db->query($sql);
    $tcount = 0;
    while ($row = $db->fetch_array($query)) {
        $rsdb[$row['name']]['asr'] = (round($rsdb[$row['name']]['callcount'] / $row[1], 3) * 100) . "%";
        $rsdb[$row['name']]['tcount'] = $row[1];
        $tcount += $row[1];
    }

    if ($tcount) {
        $asr = (round($callcount / $tcount, 3) * 100) . "%";
    }

    if ($order_key) {
        uasort($rsdb, "my_cmp");
    }
    return $rsdb;
}

$db = new DB(); //lrdtab

if (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action == "netcheck") {
        $action = "main";
        $sendbuf = my_pack2(DEV_NETCHECK, $_GET['sim_name'], TYPE_SIM);
        if (($socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP)) <= 0) {
            echo "socket_create() failed: reason: " . socket_strerror($socket) . "\n";
            exit;
        }
        $socks[] = $socket;
        //echo "s:$sendbuf,".strlen($sendbuf);
        if (socket_sendto($socket, $sendbuf, strlen($sendbuf), 0, "127.0.0.1", $phpsvrport) === false) {
            echo("sendto error");
        }

        for ($i = 0; $i < 2; $i++) {
            $read = array($socket);
            $err = socket_select($read, $write = null, $except = null, 5);
            if ($err > 0) {
                if (($n = @socket_recvfrom($socket, $buf, 1024, 0, $ip, $port)) == false) {
                    //echo("recvform error".socket_strerror($ret)."<br>");
                    continue;
                } else {
                    if ($buf == $sendbuf) {
                        $flag = 1;
                        break;
                    }
                }
            }
        }
        if (!$flag) {
            die("Mydify Success,but cannot get response from process named 'xchange' or 'scheduler'. please check process.");
        }

        $timer = 2;
        $timeout = 7;
        for (;;) {
            $read = $socks;
            flush();
            $err = socket_select($read, $write = null, $except = null, $timeout);
            if ($err === false) {
                echo "select error!";
            } elseif ($err == 0) {
                //全体超时
                if (--$timer <= 0) {
                    echo "timeout!";
                    break;
                }
            } else {
                if (($n = @socket_recvfrom($socket, $buf, 1024, 0, $ip, $port1)) == false) {
                    //echo("recvform error".socket_strerror($ret)."<br>");
                    continue;
                }
                $data = my_unpack_net_check($buf);
                $data['delay'] = floor($data['delay']) / 1000;
                echo "<script language=\"javascript\">alert('NET Check Result:\\nSlot Name:$data[sid]\\nsent:$data[sent] recv:$data[recv] lost:$data[lost] bad:$data[bad] dup:$data[dup] daley:{$data['delay']}ms')</script>";
                break;
            }
        }
    } elseif ($action == "modify") {
        $name = $_GET['sim_name'];
        $query = $db->query("select * from sim_team order by sim_team_id ");
        while ($row = $db->fetch_array($query)) {
            $prsdb[] = $row;
        }
        $rs = $db->fetch_array($db->query("SELECT * FROM sim where sim_name='$name'"));
        if ($rs['dev_disable']) {
            $ck2 = 'selected';
        } else {
            $ck1 = 'selected';
        }

        if ($rs['imei_mode'] == 1) {
            $imei_ck2 = 'selected';
        } elseif ($rs['imei_mode'] == 0) {
            $imei_ck1 = 'selected';
        } elseif ($rs['imei_mode'] == 2) {
            $imei_ck3 = 'selected';
        } elseif ($rs['imei_mode'] == 3) {
            $imei_ck4 = 'selected';
        } elseif ($rs['imei_mode'] == 4) {
            $imei_ck5 = 'selected';
        }

        if ($rs['no_ring_disable']) {
            $wa_ck1 = 'checked';
        }

        if ($rs['no_answer_disable']) {
            $wa_ck2 = 'checked';
        }

        if ($rs['short_call_disable']) {
            $wa_ck3 = 'checked';
        }

        if ($rs['count_limit_no_connect']) {
            $ci_no_connect_ck = 'checked';
        }

        if ($rs['remain_time'] < 0) {
            $rs['remain_time'] = "";
        }

        $query = $db->query("select line_name from device_line where goip_team_id='0' and line_name not in (select plan_line_name from sim where plan_line_name != '$rs[plan_line_name]') order by line_name ");
        while ($row = $db->fetch_array($query)) {
            $grsdb[] = $row;
        }
    } elseif ($action == "savemodify") { //Accion a ejecutar

        $password = $_POST['Password'];
        $name = $_POST['name'];
        $oldname = $_POST['oldname'];
        $team_id = $_POST['team_id'];
        $old_team_id = $_POST['old_team_id'];
        $id = $_GET['id'];
        $dev_disable = $_POST['dev_disable'];
        $sim_name = $_POST['sim_name'];
        $imei = $_POST['imei'];
        $remain_time = $_POST['remain_time'];
        $time_unit = $_POST['time_unit'];
        $imei_mode = $_POST['imei_mode'];
        $auto_reset_remain = $_POST['auto_reset_remain'];
        $auto_reset_remain_s = $_POST['auto_reset_remain_s'];
        if ($remain_time == "" || $remain_time < -1) {
            $remain_time = -1;
        }

        $month_remain_time = ($_POST['month_limit_time'] == -1) ? -1 : ($_POST['month_limit_time'] * 60);

        $sim_rs = $db->fetch_array($db->query("SELECT * FROM sim where sim_name='$sim_name'"));
        if ($sim_rs['time_limit'] != $_POST['time_limit']) {
            $t_remain .= ",remain_time=$_POST[time_limit]";
        }

        if ($sim_rs['count_limit'] != $_POST['count_limit']) {
            $t_remain .= ",count_remain=$_POST[count_limit]";
        }

        if ($sim_rs['limit_sms'] != $_POST['limit_sms']) {
            $t_remain .= ",remain_sms=$_POST[limit_sms]";
        }

        if ($sim_rs['month_limit_time'] != $_POST['month_limit_time']) {
            $db->query("insert into logs set sim_name='$sim_name', log='change month limit'");
            $t_remain .= ",month_remain_time=$month_remain_time,month_last_reset_time=now()";
        }
        if ($sim_rs['no_connected_limit'] != $_POST['no_connected_limit']) {
            $t_remain .= ",no_connected_remain=$_POST[no_connected_limit]";
        }

        if ($dev_disable == 0 && $sim_rs['dev_disable'] == 1) {
            $t_remain . ",no_ring_remain==if(),no_answer_remain=0,short_call_remain=0";
        }

        if ($dev_disable == 0 && $sim_rs['dev_disable'] == 1) {
            $db->query("update sim SET no_ring_remain=0,no_answer_remain=0,short_call_remain=0 where sim_name='$sim_name' and ((short_call_disable=1 and short_call_limit>0 and short_call_remain>=short_call_limit) or (no_ring_disable=1 and no_ring_limit>0 and no_ring_remain>=no_ring_limit) or (no_answer_disable=1 and no_answer_limit>0 and no_answer_remain>=no_answer_limit))");
        }

        if ($dev_disable != $sim_rs['dev_disable']) {
            if ($dev_disable == 0) {
                $db->query("insert into logs set sim_name='$sim_name', log='enabled manually by single sim'");
            } else {
                $db->query("insert into logs set sim_name='$sim_name', log='disabled manually by single sim'");
            }
        };

        /*
        $db->query("UPDATE sim
            SET
                imei_mode='$imei_mode',
                sim_team_id='$team_id',
                dev_disable='$dev_disable',
                plan_line_name='$_POST[line_name]',
                imei='$imei',
                time_limit='$_POST[time_limit]',
                time_unit='$time_unit',
                no_ring_limit='$_POST[no_ring_limit]',
                no_answer_limit='$_POST[no_answer_limit]',
                short_call_limit='$_POST[short_call_limit]',
                short_time='$_POST[short_time]',
                no_ring_disable='$_POST[no_ring_disable]',
                no_answer_disable='$_POST[no_answer_disable]',
                short_call_disable='$_POST[short_call_disable]',
                count_limit='$_POST[count_limit]',
                no_connected_limit='$_POST[no_connected_limit]',
                auto_reset_remain='$auto_reset_remain',
                auto_reset_remain_s='$auto_reset_remain_s',
                count_limit_no_connect='$_POST[count_limit_no_connect]',
                limit_sms='$_POST[limit_sms]',
                month_limit_time='$_POST[month_limit_time]',
                month_reset_day='$_POST[month_reset_day]'
                $t_remain
                WHERE sim_name='$sim_name'"
                );
                */
        $db->query("UPDATE sim SET sim_team_id='$team_id' WHERE sim_name='$sim_name'");

        $query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name where sim_name='$sim_name'");
        while ($row = $db->fetch_array($query)) {
            sim_info($row, $send);
        }
        if (!$team_id) {
            $send[] = my_pack2(DEV_BINDING, $sim_name, $_POST['line_name']);
        } else {
            $send[] = my_pack2(DEV_BINDING, $sim_name, 0);
        }
        sendto_xchanged($send);
        WriteSuccessMsg("**Modify SIM Slot successful!**\n", "");
    } elseif ($action == "moremodify") {
        $sim_name = get_id();
        if (!$sim_name) {
            WriteErrMsg("Not choose one SIM Slot!");
        }

        //$name=$_GET['name'];
        $query = $db->query("select * from sim_team order by sim_team_id ");
        while ($row = $db->fetch_array($query)) {
            $prsdb[] = $row;
        }
    } elseif ($action == "savemoremodify") {
        //print_r($_POST);
        $password = $_POST['Password'];
        $name = $_POST['name'];
        $team_id = $_POST['team_id'];
        $id = $_GET['id'];
        $dev_disable = $_POST['dev_disable'];
        $sim_name = $_POST['sim_name'];
        $imei = $_POST['imei'];
        $remain_time = $_POST['remain_time'];
        $time_unit = $_POST['time_unit'];
        $imei_mode = $_POST['imei_mode'];
        $where = gen_sim_where();
        if ($remain_time == "" || $remain_time < -1) {
            $remain_time = -1;
        }

        if ($_POST['month_limit_time'] == "" || $_POST['month_limit_time'] < -1) {
            $_POST['month_limit_time'] = -1;
        }

        $month_remain_time = ($_POST['month_limit_time'] == -1) ? -1 : ($_POST['month_limit_time'] * 60);
        if ($_POST['time_limit'] == "" || $_POST['time_limit'] < -1) {
            $_POST['time_limit'] = -1;
        }

        if ($_POST['limit_sms'] == "" || $_POST['limit_sms'] < -1) {
            $_POST['limit_sms'] = -1;
        }

        if ($_POST['no_ring_limit'] == "" || $_POST['no_ring_limit'] < -1) {
            $_POST['no_ring_limit'] = -1;
        }

        if ($_POST['no_answer_limit'] == "" || $_POST['no_answer_limit'] < -1) {
            $_POST['no_answer_limit'] = -1;
        }

        if ($_POST['short_call_limit'] == "" || $_POST['short_call_limit'] < -1) {
            $_POST['short_call_limit'] = -1;
        }

        if ($_POST['count_limit'] == "" || $_POST['count_limit'] < -1) {
            $_POST['count_limit'] = -1;
        }

        if ($_POST['no_connected_limit'] == "" || $_POST['no_connected_limit'] < -1) {
            $_POST['no_connected_limit'] = -1;
        }

        //echo $sim_name;
        $ErrMsg = "";
        if ($ErrMsg != "") {
            WriteErrMsg($ErrMsg);
        } else {
            //$sim_rs=$db->fetch_array($db->query("SELECT * FROM sim where sim_name='$sim_name'"));
            //if($sim_rs['time_limit']!=$_POST[time_limit]) $t_remain=",remain_time=$_POST[time_limit]"; //重置时间
            $sql = "UPDATE sim SET ";
            if ($_POST['team_id_modify']) {
                $sql .= "sim_team_id='$team_id',";
            }

            if ($_POST['imei_mode_modify']) {
                $sql .= "imei_mode='$imei_mode',";
            }

            if ($_POST['month_limit_time_modify']) {
                $sql .= "month_limit_time='$_POST[month_limit_time]',month_remain_time='$month_remain_time',month_last_reset_time=now(),";
            }

            if ($_POST['month_reset_day_modify']) {
                $sql .= "month_reset_day='$_POST[month_reset_day]',";
            }

            if ($_POST['time_limit_modify']) {
                $sql .= "time_limit='$_POST[time_limit]',remain_time='$_POST[time_limit]',";
            }

            if ($_POST['count_limit_modify']) {
                $sql .= "count_limit='$_POST[count_limit]',count_remain=$_POST[count_limit],";
            }

            if ($_POST['limit_sms_modify']) {
                $sql .= "limit_sms='$_POST[limit_sms]',remain_sms='$_POST[limit_sms]',";
            }

            if ($_POST['count_limit_no_connect_modify']) {
                $sql .= "count_limit_no_connect='$_POST[count_limit_no_connect]',";
            }

            if ($_POST['no_connected_limit_modify']) {
                $sql .= "no_connected_limit='$_POST[no_connected_limit]',no_connected_remain=$_POST[no_connected_limit],";
            }

            if ($_POST['time_unit_modify']) {
                $sql .= "time_unit='$time_unit',";
            }

            if ($_POST['no_ring_modify']) {
                $sql .= "no_ring_limit='$_POST[no_ring_limit]',no_ring_disable='$_POST[no_ring_disable]',";
            }

            if ($_POST['no_answer_modify']) {
                $sql .= "no_answer_limit='$_POST[no_answer_limit]',no_answer_disable='$_POST[no_answer_disable]',";
            }

            if ($_POST['short_call_modify']) {
                $sql .= "short_call_limit='$_POST[short_call_limit]',short_call_disable='$_POST[short_call_disable]',";
            }

            if ($_POST['short_time_modify']) {
                $sql .= "short_time='$_POST[short_time]',";
            }

            if ($_POST['dev_disable_modify']) {
                $sql .= "dev_disable='$dev_disable',";
            }

            if ($_POST['auto_reset_remain_modify']) {
                $sql .= "auto_reset_remain='$_POST[auto_reset_remain]',";
            }

            if ($_POST['auto_reset_remain_s_modify']) {
                $sql .= "auto_reset_remain_s='$_POST[auto_reset_remain_s]',";
            }

            $sql .= "sim_login=sim_login $where";
            if ($_POST['dev_disable_modify'] && $dev_disable == 0) {
                $db->query("update sim SET no_ring_remain=0,no_answer_remain=0,short_call_remain=0 $where and dev_disable=1 and ((short_call_disable=1 and short_call_limit>0 and short_call_remain>=short_call_limit) or (no_ring_disable=1 and no_ring_limit>0 and no_ring_remain>=no_ring_limit) or (no_answer_disable=1 and no_answer_limit>0 and no_answer_remain>=no_answer_limit))");
            }

            if ($_POST['dev_disable_modify']) {
                $query = $db->query("select sim_name from sim $where and dev_disable!='$dev_disable'");
                while ($row = $db->fetch_array($query)) {
                    if ($dev_disable == 0) {
                        $db->query("insert into logs set sim_name='$row[sim_name]', log='enabled batch manually'");
                    } else {
                        $db->query("insert into logs set sim_name='$row[sim_name]', log='disabled batch manually'");
                    }
                }
            };
            $db->query($sql);

            $query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name $where");
            while ($row = $db->fetch_array($query)) {
                if ($_POST['month_reset_day_modify']) {
                    $db->query("insert into logs set sim_name='$row[sim_name]', log='change month limit'");
                }

                sim_info($row, $send);
                /*
           if($row['imei_mode']==4) $row['imei_mode']=2;
           if($row['imei']==NULL) $row['imei'] = "000000000000000";
           $imei_len=strlen($row['imei']);
           if($imei_len<15) $row['imei'] = sprintf("%15s",$row['imei']."000000000000000");
           if($row['remain_time']==NULL || $row['remain_time'] <-1 ) $row['remain_time'] = -1;
           if($row['month_remain_time']==NULL || $row['month_remain_time'] <-1 ) $row['month_remain_time'] = -1;
           if($row['month_remain_time']==0) $row['dev_disable']=1;
           $send[]=my_pack($row, SIM_ADD);
           if($_POST['dev_disable_modify'] || $_POST['month_limit_time_modify']) $send[]=my_pack2($row[dev_disable]?DEV_DISABLE:DEV_ENABLE, $row[sim_name], TYPE_SIM);
           if($row['imei_mode']==3) $send[]=my_pack($row, IMEI_IMSI_INFO);
            */
                if ($_POST['team_id_modify'] && $team_id) {
                    $send[] = my_pack2(DEV_BINDING, $row['sim_name'], 0);
                }
            }

            sendto_xchanged($send);
            WriteSuccessMsg("**Modify SIM Slot successful!**\n", "?bank_name={$_REQUEST['bank_name']}&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
        } // hasta aquí
    } elseif ($action == "reboot") {
        $send[] = my_pack2(MACHINE_REBOOT, $_GET['name'], TYPE_SIM);
        sendto_xchanged($send);
        WriteSuccessMsg("<br><li>The commond is sended to SIM Slot($_GET[name]).</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
    } elseif ($action == "reset_limit") {
        $where = gen_sim_where();
        $db->query("update sim set no_ring_remain=0,no_answer_remain=0,short_call_remain=0,remain_time=time_limit,remain_sms=limit_sms,count_remain=count_limit,no_connected_remain=no_connected_limit,month_remain_time=if(month_limit_time=-1, -1, month_limit_time*60) $where");

        $query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name $where");
        while ($row = $db->fetch_array($query)) {
            $db->query("insert into logs set sim_name='$row[sim_name]', log='reset limit from web'");
            sim_info($row, $send);
            /*
        if($row['imei_mode']==4) $row['imei_mode']=2;
        if($row['imei']==NULL) $row['imei'] = "000000000000000";
        $imei_len=strlen($row['imei']);
        if($imei_len<15) $row['imei'] = sprintf("%15s",$row['imei']."000000000000000");
        if($row['remain_time']==NULL || $row['remain_time'] <-1 ) $row['remain_time'] = -1;
        if($row['month_remain_time']==NULL || $row['month_remain_time'] <-1 ) $row['month_remain_time'] = -1;
        if($row['month_remain_time']==0) $row['dev_disable']=1;
        $send[]=my_pack($row, SIM_ADD);
        $send[]=my_pack2($row[dev_disable]?DEV_DISABLE:DEV_ENABLE, $row[sim_name], TYPE_SIM);
         */
            //if($row['imei_mode']==3) $send[]=my_pack($row, IMEI_IMSI_INFO);
            //$send[]=my_pack2(RESET_LIMIT, $row['sim_name']);
        }
        sendto_xchanged($send);
        WriteSuccessMsg("<br><li>The reset limit commond is sended to Server.($sim_name)</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
    } elseif ($action == "reset_period") {
        $where = gen_sim_where();

        $query = $db->query("select sim_name from sim $where");
        while ($row = $db->fetch_array($query)) {
            $db->query("insert into logs set sim_name='$row[sim_name]', log='reset period limit from web'");
            $send[] = my_pack2(RESET_LIMIT, $row['sim_name']);
        }
        sendto_xchanged($send);
        WriteSuccessMsg("<br><li>The commond is sended to Server.($sim_name)</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
    } elseif ($action == "awaken") {
        $where = gen_sim_where();

        $query = $db->query("select sim_name from sim $where");
        while ($row = $db->fetch_array($query)) {
            $send[] = my_pack2(DEV_ACTIVING, $row['sim_name'], TYPE_SIM);
        }
        sendto_xchanged($send);
        WriteSuccessMsg("<br><li>The commond is sended to Server.</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
    } elseif ($action == "set_imei_db") {
        $where = gen_sim_where();

        $query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name $where");
        $num = $db->num_rows($query);
        $query0 = $db->query("select id,imei from imei_db order by used limit $num");
        while ($row0 = $db->fetch_array($query0)) {
            $imei_db[] = $row0;
        }
        $i = 0;
        while ($row = $db->fetch_array($query)) {
            if ($imei_db[$i]) {
                $row['imei_mode'] == 2;
                $row['imei'] = $imei_db[$i]['imei'];
                $imei_db[$i]['sim_name'] = $row['sim_name'];
                $log .= "<br>SIM Slot:$row[sim_name] Set IMEI:$row[imei]";
                $send[] = my_pack($row, SIM_ADD);
                $db->query("update imei_db set sim_name='$row[sim_name]',used=used+1,used_time=now() where id=" . $imei_db[$i]['id']);
                $db->query("update sim set imei_mode=2, imei='$row[imei]' where id=$row[id]");
            } else {
                $log .= "<br>SIM Slot:$row[sim_name] not find IMEI from database";
            }
            $i++;
        }
        sendto_xchanged($send);
        WriteSuccessMsg("<br><li>The commond is sended to Server.$log</li>", "?bank_name=$_REQUEST[bank_name]&group_id=$_REQUEST[group_id]&order=$order_type&order_key=$order_key");
    }
//else $action="main";

} else {
    $action = "main";
}
