<?php

require_once 'global.php';

function savemodify($lr_get, $lr_post)
{
    define("OK", true);
    $db = new DB();
    $_GET = $lr_get;
    $_POST = $lr_post;
    $_REQUEST = array_merge($_GET, $_POST);

    if (!$_REQUEST['order']) {
        $_REQUEST['order'] = "asc";
    }
    if ($_REQUEST['order'] == "desc") {
        $order_type2 = 'asc';
    } else {
        $order_type2 = 'desc';
    }

    if (!$_REQUEST['order_key']) {
        $_REQUEST['order_key'] = "sim_name";
    }

    $order_type = $_REQUEST['order'];
    $order_key = $_REQUEST['order_key'];

    $t_remain = '';
    $password = $_POST['Password'];
    $name = $_POST['name'];
    $oldname = $_POST['oldname'];
    $team_id = $_POST['team_id'];
    $old_team_id = $_POST['old_team_id'];
    $id = $_GET['id'];
    $dev_disable = $_POST['dev_disable'];
    $sim_name = $_POST['sim_name'];
    $imei = $_POST['imei'];
    $remain_time = $_POST['remain_time'];
    $time_unit = $_POST['time_unit'];
    $imei_mode = $_POST['imei_mode'];
    $auto_reset_remain = $_POST['auto_reset_remain'];
    $auto_reset_remain_s = $_POST['auto_reset_remain_s'];


    if ($remain_time == "" || $remain_time < -1) {
        $remain_time = -1;
    }

    $month_remain_time = ($_POST['month_limit_time'] == -1) ? -1 : ($_POST['month_limit_time'] * 60);

    $sim_rs = $db->fetch_array($db->query("SELECT * FROM sim where sim_name='$sim_name'"));
    if ($sim_rs['time_limit'] != $_POST['time_limit']) {
        $t_remain .= ",remain_time=$_POST[time_limit]";
    }

    if ($sim_rs['count_limit'] != $_POST['count_limit']) {
        $t_remain .= ",count_remain=$_POST[count_limit]";
    }

    if ($sim_rs['limit_sms'] != $_POST['limit_sms']) {
        $t_remain .= ",remain_sms=$_POST[limit_sms]";
    }

    if ($sim_rs['month_limit_time'] != $_POST['month_limit_time']) {
        $db->query("insert into logs set sim_name='$sim_name', log='change month limit'");
        $t_remain .= ",month_remain_time=$month_remain_time,month_last_reset_time=now()";
    }
    if ($sim_rs['no_connected_limit'] != $_POST['no_connected_limit']) {
        $t_remain .= ",no_connected_remain=$_POST[no_connected_limit]";
    }

    if ($dev_disable == 0 && $sim_rs['dev_disable'] == 1) {
        $t_remain . ",no_ring_remain==if(),no_answer_remain=0,short_call_remain=0";
    }

    if ($dev_disable == 0 && $sim_rs['dev_disable'] == 1) {
        $db->query("update sim SET no_ring_remain=0,no_answer_remain=0,short_call_remain=0 where sim_name='$sim_name' and ((short_call_disable=1 and short_call_limit>0 and short_call_remain>=short_call_limit) or (no_ring_disable=1 and no_ring_limit>0 and no_ring_remain>=no_ring_limit) or (no_answer_disable=1 and no_answer_limit>0 and no_answer_remain>=no_answer_limit))");
    }

    if ($dev_disable != $sim_rs['dev_disable']) {
        if ($dev_disable == 0) {
            $db->query("insert into logs set sim_name='$sim_name', log='enabled manually by single sim'");
        } else {
            $db->query("insert into logs set sim_name='$sim_name', log='disabled manually by single sim'");
        }
    };

    $db->query("UPDATE sim SET sim_team_id='$team_id' WHERE sim_name='$sim_name'");

    $query = $db->query("select sim.*,password from sim left join sim_bank on sim.bank_name=sim_bank.name where sim_name='$sim_name'");
    while ($row = $db->fetch_array($query)) {
        sim_info($row, $send);
    }
    if (!$team_id) {
        $send[] = my_pack2(DEV_BINDING, $sim_name, $_POST['line_name']);
    } else {
        $send[] = my_pack2(DEV_BINDING, $sim_name, 0);
    }
    sendto_xchanged($send);
    WriteSuccessMsg("**Modify SIM Slot successful!**\n", "LRDTAB");
}


function get_id()
{
    if ($_REQUEST['chkAll0']) {
        return "all SIM Slots";
    }

    $num = $_REQUEST['boxs'];
    $id = '';
    for ($i = 0; $i < $num; $i++) {
        if (!empty($_REQUEST["Id$i"])) {
            if ($id == "") {
                $id = $_REQUEST["Id$i"];
            } else {
                $id = $_REQUEST["Id$i"] . ",$id";
            }
        }
    }
    if ($_REQUEST['rstr']) {
        if ($id == "") {
            $id = $_REQUEST['rstr'];
        } else {
            $id = $_REQUEST['rstr'] . ",$id";
        }
    }
    return $id;
}

function gen_sim_where()
{
    if ($_REQUEST['chkAll0']) {
        $where = " where 1";
    } elseif ($_REQUEST['sim_name']) {
        $where = " where sim_name in (" . $_REQUEST['sim_name'] . ") ";
    } else {
        $id = get_id($_REQUEST);
        if (!$id) {
            WriteErrMsg("Not choose one SIM Slot!");
        }

        $where = " where sim_name in ($id)";
    }
    return $where;
}




function my_cmp($a, $b, $order_type, $order_key)
{
    if ($a[$order_key] == $b[$order_key]) {
        return 0;
    } elseif ($order_type == "desc") {
        return ($a[$order_key] < $b[$order_key]) ? 1 : -1;
    } else {
        return ($a[$order_key] > $b[$order_key]) ? 1 : -1;
    }
}



function second_to_time($t)
{
    $h = floor($t / 3600);
    $m = floor(($t - 3600 * $h) / 60);
    $s = $t - 3600 * $h - 60 * $m;
    $n = '';
    if ($h) {
        $n .= "{$h}h ";
    }

    if ($m) {
        $n .= "{$m}m ";
    }

    $n .= "{$s}s";
    return $n;
}


function get_rsdb($db)
{
    $_REQUEST = [];
    if (!$_REQUEST['order']) {
        $_REQUEST['order'] = "asc";
    }
    if ($_REQUEST['order'] == "desc") {
        $order_type2 = 'asc';
    } else {
        $order_type2 = 'desc';
    }

    if (!$_REQUEST['order_key']) {
        $_REQUEST['order_key'] = "name";
    }

    $order_type = $_REQUEST['order'];
    $order_key = $_REQUEST['order_key'];

    if ($_REQUEST['submit_value'] == '1 Hour') {
        $start_time = date("Y-m-d H:i", time() - 3600);
        $end_time = date("Y-m-d H:i");
    } elseif ($_REQUEST['submit_value'] == '30 Min') {
        $start_time = date("Y-m-d H:i", time() - 1800);
        $end_time = date("Y-m-d H:i");
    } else {
        $start_time = $_REQUEST['start_time'];
        if (!$start_time) {
            $start_time = date("Y-m-d") . " 00:00";
        }

        $end_time = $_REQUEST['end_time'];
        if (!$end_time) {
            $end_time = date("Y-m-d H:i");
        }
    }

    $type = $_REQUEST['type'];
    if (!$type) {
        $type = "sim";
    }

    $name = $_REQUEST['name'];

    if (!$name) {
        $ch = "selected";
        $name_ch = "All";
    }
    if ($name) {
        $wh = "and " . $type . "_name=$name";
    }

    $select = "<select name=\"name\"  style=\"width:80px\" >\n\t<option value=\"0\" $ch>All</option>\n";
    if ($type == "line") {
        $query = $db->query("select id, " . $type . "_name as name from device_line where 1  order by line_name");
    } else {
        $query = $db->query("select id, " . $type . "_name as name from sim where 1 order by sim_name");
    }

    while ($row = $db->fetch_array($query)) {
        if ($name == $row['name']) {
            $row['ch'] = "selected";
            $name_ch = $row['name'];
            $channel[] = $row;
            $rsdb[$row['name']] = $row;
        } elseif (!$name) {
            $channel[] = $row;
            $rsdb[$row['name']] = $row;
        }
        //$rsdb[$row['name']]=$row;
        $select .= "\t<option value=\"{$row['name']}\" $row[ch]>{$row['name']}</option>\n";
    }
    $select .= "</select>";

    $sql = "SELECT " . $type . "_name as name,sum(duration) as calltime,count(id) as callcount from call_record where dir=0 and duration>0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";
    $query = $db->query($sql);
    $calltime = 0;
    $callcount = 0;
    while ($row = $db->fetch_array($query)) {
        $calltime += $row[1];
        $callcount += $row[2];
        $row['acd'] = round($row[1] / $row[2]);
        $row['acd_s'] = second_to_time($row['acd']);
        $row['calltime_s'] = second_to_time($row[1]);
        $rsdb[$row['name']] = $row;
    }
    $calltime_s = second_to_time($calltime);
    if ($callcount) {
        $acd = round($calltime / $callcount);
    }

    $acd_s = second_to_time($acd);

    $sql = "SELECT " . $type . "_name as name,count(id) from call_record where dir=0 and duration>=0 and time>'$start_time' and time<'$end_time' $wh group by " . $type . "_name";

    $query = $db->query($sql);
    $tcount = 0;
    while ($row = $db->fetch_array($query)) {
        $rsdb[$row['name']]['asr'] = (round($rsdb[$row['name']]['callcount'] / $row[1], 3) * 100) . "%";
        $rsdb[$row['name']]['tcount'] = $row[1];
        $tcount += $row[1];
    }

    if ($tcount) {
        $asr = (round($callcount / $tcount, 3) * 100) . "%";
    }

    if ($order_key) {
        uasort($rsdb, "my_cmp");
    }
    return $rsdb;
}
