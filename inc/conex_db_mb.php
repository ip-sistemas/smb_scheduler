<?php
require_once '../inc/config.inc.php';

function sql_mb($sql){
    global $dbhost_mb;
    global $dbuser_mb;
    global $dbpw_mb;
    global $dbname_mb;
    global $dbport_mb;
    exec("mysql -B -h {$dbhost_mb} -P {$dbport_mb} -u {$dbuser_mb} -p{$dbpw_mb} {$dbname_mb} -e '{$sql}' ", $salida, $codigo); 
    if ($codigo!==0) {
        echo " *** ERROR DE ACCESO A LA BASE DE DATOS MBILLING *** ";
    }
}

function sql2array_mb($sql)
{
    global $dbhost_mb;
    global $dbuser_mb;
    global $dbpw_mb;
    global $dbname_mb;
    global $dbport_mb;
    exec("mysql -B -h {$dbhost_mb} -P {$dbport_mb} -u {$dbuser_mb} -p{$dbpw_mb} {$dbname_mb} -e '{$sql}' ", $salida, $codigo);
    if ($codigo === 0) {
        $fila = explode("\t", trim($salida[0]));
        $claves = array_map('trim', $fila);
        array_shift($salida);
        $resultado = [];
        foreach ($salida as $fila) {
            $datos = explode("\t", trim($fila));
            $fila_asociativa = array();
            foreach ($claves as $indice => $clave) {
                $fila_asociativa[$clave] = $datos[$indice];
            }
            $resultado[] = $fila_asociativa;
        }
    } else {
        echo " *** ERROR DE ACCESO A LA BASE DE DATOS MBILLING *** ";
        $resultado = [];
    }
    return $resultado;
}
